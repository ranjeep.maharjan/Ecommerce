﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.EF.Models
{
    public class SubCategories
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int? CategoryId { get; set; }
        public Categories Categories { get; set; }
        public ICollection<Products> Products { get; set; }
    }
}
