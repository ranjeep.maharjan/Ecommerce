﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface ICategoryRepository : IRepository<Categories>
    {
        
    }
}
