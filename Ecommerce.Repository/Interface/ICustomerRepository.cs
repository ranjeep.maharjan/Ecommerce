﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface ICustomerRepository : IRepository<Customers>
    {
    }
}
