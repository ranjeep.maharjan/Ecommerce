﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface IEmployeeRepository : IRepository<Employees>
    {
    }
}
