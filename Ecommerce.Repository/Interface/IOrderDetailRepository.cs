﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface IOrderDetailRepository : IRepository<OrderDetails>
    {
    }
}
