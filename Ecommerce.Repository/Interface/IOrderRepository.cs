﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface IOrderRepository : IRepository<Orders>
    {
    }
}
