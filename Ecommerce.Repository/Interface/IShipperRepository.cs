﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface IShipperRepository : IRepository<Shippers>
    {
    }
}
