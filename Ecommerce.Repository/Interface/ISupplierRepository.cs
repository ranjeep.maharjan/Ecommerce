﻿using Ecommerce.EF.Models;

namespace Ecommerce.Repository.Interface
{
    public interface ISupplierRepository : IRepository<Suppliers>
    {
    }
}
