﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class CategoryRepository : Repository<Categories>, ICategoryRepository
    {
        public CategoryRepository(NorthwindContext context) : base(context)
        {

        }
    }
} 