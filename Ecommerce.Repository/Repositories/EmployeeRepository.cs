﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class EmployeeRepository : Repository<Employees>, IEmployeeRepository
    {
        public EmployeeRepository(NorthwindContext context) : base(context)
        {
        }
    }
}
