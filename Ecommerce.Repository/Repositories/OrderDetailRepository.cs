﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class OrderDetailRepository : Repository<OrderDetails>, IOrderDetailRepository
    {
        public OrderDetailRepository(NorthwindContext context) : base(context)
        {
        }
    }
}
