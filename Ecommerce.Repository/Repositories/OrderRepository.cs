﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class OrderRepository : Repository<Orders>, IOrderRepository
    {
        public OrderRepository(NorthwindContext context) : base(context)
        {
        }
    }
}
