﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class ProductRepository : Repository<Products>, IProductRepository
    {
        public ProductRepository(NorthwindContext context) : base(context)
        {
        }
    }
}
