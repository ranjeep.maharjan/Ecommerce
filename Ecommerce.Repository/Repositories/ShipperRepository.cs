﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;

namespace Ecommerce.Repository.Repositories
{
    public class ShipperRepository : Repository<Shippers>, IShipperRepository
    {
        public ShipperRepository(NorthwindContext context) : base(context)
        {
        }
    }
}
