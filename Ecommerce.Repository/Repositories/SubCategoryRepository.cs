﻿using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.Repository.Repositories
{
    public class SubCategoryRepository : Repository<SubCategories>, ISubCategoryRepository
    {
        public SubCategoryRepository(NorthwindContext context) : base(context)
        {

        }
    }
}
