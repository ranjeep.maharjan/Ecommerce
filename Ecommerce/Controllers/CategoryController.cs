﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;
using Ecommerce.Repository.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepo;
        public CategoryController(IUnitOfWork unitOfWork)
        {
            _categoryRepo = unitOfWork.Categories;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<Categories>> Get()
        {
            var categories = _categoryRepo.GetAll();

            return categories.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public ActionResult<Categories> Get(int id)
        {
            var category = _categoryRepo.Get(id);

            if (category != null)
                return category;
            else
                return NotFound();
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Create([FromBody]Categories category)
        {
            _categoryRepo.Add(category);
            _categoryRepo.SaveChanges();

            var newCategory = _categoryRepo.Find(c => c.CategoryName == category.CategoryName).FirstOrDefault();

            return CreatedAtAction(nameof(Get), new { id = newCategory.CategoryId}, newCategory);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Categories category)
        {
            if(id != category.CategoryId)
            {
                return BadRequest();
            }
            else
            {
                _categoryRepo.Update(category);
                _categoryRepo.SaveChanges();
            }

            return NoContent();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var category = _categoryRepo.Get(id);
            if(category == null)
            {
                return NotFound();
            }
            else
            {
                _categoryRepo.Remove(category);
                _categoryRepo.SaveChanges();
            }

            return NoContent();
        }
    }
}
