﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.EF.Models;
using Ecommerce.Repository.Interface;
using Ecommerce.Repository.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubCategoryController : ControllerBase
    {
        private readonly ISubCategoryRepository _subCategoryRepo;
        private readonly IUnitOfWork _unitOfWork;

        public SubCategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _subCategoryRepo = _unitOfWork.SubCategories;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SubCategories>> Get()
        {
            var subCategories = _subCategoryRepo.GetAll();
            if (subCategories == null)
                return NotFound();
            else
                return subCategories.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<SubCategories> Get(int id)
        {
            var subCategory = _subCategoryRepo.Get(id);
            if (subCategory == null)
                return NotFound();
            else
                return subCategory;

        }

        [HttpPost]
        public IActionResult Create([FromBody] SubCategories subCategories)
        {
            _subCategoryRepo.Add(subCategories);
            _subCategoryRepo.SaveChanges();

            var newSubCategory = _subCategoryRepo.Find(sc => sc.Name == subCategories.Name).FirstOrDefault();

            return CreatedAtAction(nameof(Get), newSubCategory.ID, newSubCategory);
        }

        [HttpPut]
        public IActionResult Put(int id, [FromBody] SubCategories subCategory)
        {
            if (id != subCategory.ID)
            {
                return BadRequest();
            }
            else
            {
                _subCategoryRepo.Update(subCategory);
                _subCategoryRepo.SaveChanges();
                return NoContent();
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var subCategory = _subCategoryRepo.Get(id);
            if(subCategory == null)
            {
                return NotFound();
            }
            else
            {
                _subCategoryRepo.Remove(subCategory);
                _subCategoryRepo.SaveChanges();
                return NoContent();
            }
        }
    }
}